from IPython.display import HTML
HTML('<iframe src=http://www.scholarpedia.org/article/Hopfield_network width=1000 height=350></iframe>')

import numpy as np
import os
import glob
import pylab as plt

def from_jpg(name):
    from PIL import Image
    size=40,40
    im = Image.open(name).convert('L')
    jm = im.resize(size,Image.ANTIALIAS)
    vals=np.array(jm.getdata())
    sgn = np.vectorize(lambda x: -1 if x<(255/2) else +1)
    return sgn(vals)

def to_pattern(letter):
    from numpy import array
    return array([+1 if c=='X' else -1 for c in letter.replace('\n','')])
    
def display(pattern):
    from pylab import imshow, cm, show
    side=int(np.sqrt(len(pattern)))    
    imshow(pattern.reshape((side,side)),cmap=cm.binary, interpolation='nearest')
    show()

def train(patterns):
    from numpy import zeros, outer, diag_indices
    r,c = patterns.shape
    W = zeros((c,c))
    for p in patterns:
        W = W + outer(p,p)
    W[diag_indices(c)] = 0
    return W/r
    
def recall(W, patterns, steps=5):
    from numpy import vectorize, dot
    sgn = vectorize(lambda x: -1 if x<0 else +1)
    for _ in range(steps):
        patterns = sgn(dot(patterns,W))
    return patterns
    
def degrade(patterns,noise):
    sgn=np.vectorize(lambda x: x*-1 if np.random.random()<noise else x)
    out=sgn(patterns)
    return out

def degrade_weights(W,noise):
    sgn=np.vectorize(lambda x: 0 if np.random()<noise else x)
    return sgn(W)

def makepartial(p,proportion):
    u=int(proportion*len(p))
    new_p=p
    new_p[:u]=-1
    return new_p

files = glob.glob(os.path.join('patterns','*.jpg'))

patterns=np.array([from_jpg(p) for p in files])

side=int(np.sqrt(len(patterns[0])))

f, axarr = plt.subplots(1,len(patterns))

for p in range(len(patterns)):
    axarr[p].imshow(patterns[p].reshape((side,side)), cmap="binary")
    

plt.setp([a.get_xticklabels() for a in axarr[:]], visible=False)
plt.setp([a.get_yticklabels() for a in axarr[:]], visible=False)
plt.suptitle('Our training patterns')
plt.savefig('trainingpatterns.png')

print("train weights")

W = train(patterns) 

print("test with originals")

f, axarr = plt.subplots(2, len(patterns))

for p in range(len(patterns)):
    axarr[0, p].imshow(patterns[p].reshape((side,side)), cmap="binary")
    axarr[0, p].set_title('Cue' + str(p))
    
    axarr[1, p].imshow(recall(W,patterns)[p].reshape((side,side)), cmap="binary")
    axarr[1, p].set_title('Recall' + str(p))

plt.setp([a.get_xticklabels() for a in axarr[0, :]], visible=False)
plt.setp([a.get_xticklabels() for a in axarr[1, :]], visible=False)
plt.setp([a.get_yticklabels() for a in axarr[:, 0]], visible=False)
plt.setp([a.get_yticklabels() for a in axarr[:, 1]], visible=False)
plt.suptitle('Test with training patterns')
plt.savefig('fullcue.png')

noise=0.1

patterns=np.array([from_jpg(p) for p in files])

print ("degrade patterns with noise")
testpatterns=degrade(patterns,noise)

f, axarr = plt.subplots(2, len(patterns))

for p in range(len(testpatterns)):
    axarr[0, p].imshow(testpatterns[p].reshape((side,side)), cmap="binary")
    axarr[0, p].set_title('Cue' + str(p))
    
    axarr[1, p].imshow(recall(W,testpatterns)[p].reshape((side,side)), cmap="binary")
    axarr[1, p].set_title('Recall' + str(p))

plt.setp([a.get_xticklabels() for a in axarr[0, :]], visible=False)
plt.setp([a.get_xticklabels() for a in axarr[1, :]], visible=False)
plt.setp([a.get_yticklabels() for a in axarr[:, 0]], visible=False)
plt.setp([a.get_yticklabels() for a in axarr[:, 1]], visible=False)
plt.suptitle('Test with noisy cue')
plt.savefig('noisycue.png')

print("test with partial cues")

proportion=0.4

patterns=np.array([from_jpg(p) for p in files]) 
   
testpatterns=[makepartial(p,proportion) for p in patterns]


f, axarr = plt.subplots(2, len(testpatterns))

for p in range(len(testpatterns)):
    axarr[0, p].imshow(testpatterns[p].reshape((side,side)), cmap="hot")
    axarr[0, p].set_title('Cue' + str(p))
    
    axarr[1, p].imshow(recall(W,testpatterns)[p].reshape((side,side)), cmap="hot")
    axarr[1, p].set_title('Recall' + str(p))

plt.setp([a.get_xticklabels() for a in axarr[0, :]], visible=False)
plt.setp([a.get_xticklabels() for a in axarr[1, :]], visible=False)
plt.setp([a.get_yticklabels() for a in axarr[:, 0]], visible=False)
plt.setp([a.get_yticklabels() for a in axarr[:, 1]], visible=False)
plt.suptitle('Test with partial cue')
plt.savefig('partialcue.png')

